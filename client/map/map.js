import { Template } from 'meteor/templating';
import '../../lib/plugins/hexGridWidget.js';
import './map.html';

let hexGrid = function(hexRadius, rows, columns, cssClass) {
    $('#container').hexGridWidget(hexRadius, rows, columns, cssClass);
}

Template.map.helpers({
    hexGrid: hexGrid
});

Template.map.onCreated(function() {
    if (!this.data.hexRadius) this.data.hexRadius = 10;
    if (!this.data.columns) this.data.columns = 20;
    if (!this.data.rows) this.data.rows = 20;
    if (!this.data.cssClass) this.data.cssClass = 'hex-field';
});

Template.map.onRendered(function() {
    hexGrid(this.data.hexRadius, this.data.rows, this.data.columns, this.data.cssClass);
});
