Package.describe({
    name: 'angle:hex-map',
    version: '0.0.1',
    // Brief, one-line summary of the package.
    summary: 'A simple jQuery Hex grid Widget, implemented using SVG, for prototyping hex-grid' +
    'based games.',
    // URL to the Git repository containing the source code for this package.
    git: 'https://gitlab.com/AngularAngel/meteor-hex-map',
    // By default, Meteor will default to using README.md for documentation.
    // To avoid submitting documentation, set this field to null.
    documentation: 'README.md'
});

Package.onUse(function(api) {
    api.versionsFrom('1.8.1');
    api.use('ecmascript');

    api.use([
        'templating@1.3.2',
    ], 'client');

    api.addFiles([
        'lib/plugins/hexGridWidget.js',
        'client/map/map.html',
        'client/map/map.css',
        'client/map/map.js'
    ], 'client');
});

Package.onTest(function(api) {
    api.use('ecmascript');
    api.use('tinytest');
    api.use('angle:hex-map');
});
